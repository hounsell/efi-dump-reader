﻿using System;
using System.Text;

namespace EfiDumpReader
{
    public struct EfiVariable
    {
        public string Name { get; }
        public EfiVariableAttributes Attributes { get; }
        public Guid Type { get; }
        public ReadOnlyMemory<byte> Data { get; }
        public uint Crc { get; }
        public EfiVariable(ReadOnlyMemory<byte> record, int nameSize, int dataSize)
        {
            Name = Encoding.Unicode.GetString(record.Slice(0, nameSize - 2).Span);
            Type = new Guid(record.Slice(nameSize, 16).Span);
            Attributes = (EfiVariableAttributes)(BitConverter.ToUInt32(record.Slice(nameSize + 16, sizeof(uint)).Span));
            Data = record.Slice(nameSize + 20, dataSize);
            Crc = BitConverter.ToUInt32(record.Slice(nameSize + 20 + dataSize, sizeof(uint)).Span);
        }
    }
}
