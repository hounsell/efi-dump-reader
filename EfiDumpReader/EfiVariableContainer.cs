﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Text;

namespace EfiDumpReader
{
    public class EfiVariableContainer
    {
        public IReadOnlyList<EfiVariable> Entries { get; }

        public EfiVariableContainer(string path)
        {
            byte[] content;
            using (var fStr = new FileStream(path, FileMode.Open, FileAccess.Read))
            {
                content = new byte[fStr.Length];
                fStr.Read(content, 0, content.Length);
            }

            int position = 0;

            var entries = ImmutableArray.CreateBuilder<EfiVariable>();
            while (position < content.Length)
            {
                int nameSize = BitConverter.ToInt32(content.AsSpan(position, sizeof(int)));
                position += sizeof(uint);

                int dataSize = BitConverter.ToInt32(content.AsSpan(position, sizeof(int)));
                position += sizeof(uint);

                var recordSize = nameSize + 16 /* size of EFI_GUID */ + sizeof(uint) + dataSize + sizeof(uint);
                var record = new ReadOnlyMemory<byte>(content, position, recordSize);
                position += recordSize;

                entries.Add(new EfiVariable(record, nameSize, dataSize));
            }

            Entries = entries.ToImmutable();
        }
    }
}
