﻿using System;

namespace EfiDumpReader
{
    [Flags]
    public enum EfiVariableAttributes : uint
    {
        NON_VOLATILE = 0x1,
        BOOTSERVICE_ACCESS = 0x2,
        RUNTIME_ACCESS = 0x4,
        HARDWARE_ERROR_RECORD = 0x8,
        AUTHENTICATED_WRITE_ACCESS = 0x10,
        TIME_BASED_AUTHENTICATED_WRITE_ACCESS = 0x20,
        APPEND_WRITE = 0x40,
        ENHANCED_AUTHENTICATED_ACCESS = 0x80
    }
}
