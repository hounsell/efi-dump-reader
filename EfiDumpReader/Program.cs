﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace EfiDumpReader
{
    class Program
    {
        static void Main(string[] args)
        {
            string path;
            if (args.Length > 1)
            {
                path = args[1];
            }
            else
            {
                Console.Write("Path: ");
                path = Console.ReadLine();
            }

            var container = new EfiVariableContainer(path);

            foreach (var entry in container.Entries)
            {
                Console.Write("Name:\t\t");
                Console.WriteLine(entry.Name);

                Console.Write("Type:\t\t");
                Console.WriteLine(entry.Type);

                Console.Write("Attributes:\t");
                Console.WriteLine(entry.Attributes);

                Console.Write("Value:\t\t");

                char[] dataText = new char[entry.Data.Length];

                Span<char> dataSpan = dataText.AsSpan();
                Encoding.Unicode.GetChars(entry.Data.Span, dataSpan);
                bool isText = true;
                foreach (var c in dataSpan)
                {
                    isText &= ((c >= ' ' && c <= '}') || c == 0);
                }

                string dataString = new string(dataSpan);
                if (isText && !string.IsNullOrWhiteSpace(dataString))
                {
                    Console.WriteLine(dataString);
                }
                else if (entry.Data.Length > 32)
                {
                    Console.WriteLine("({0} bytes)", entry.Data.Length);
                }
                else
                {
                    Console.WriteLine(BitConverter.ToString(entry.Data.ToArray()));
                }

                Console.WriteLine("CRC:\t\t{0:X8}", entry.Crc);

                Console.WriteLine();
            }
        }
    }
}
